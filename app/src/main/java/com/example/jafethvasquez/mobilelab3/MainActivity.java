package com.example.jafethvasquez.mobilelab3;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

public class MainActivity extends AppCompatActivity {

    private static final int PHOTO_CAPTURE = 101;

    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private StorageReference mStorageRef;
    Uri pictureUri;
    EditText username;
    EditText password;
    EditText email;
    ImageView picture;
    Button btn_registrar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        username = findViewById(R.id.log_name);
        email = findViewById(R.id.log_email);
        password = findViewById(R.id.log_password);
        picture = findViewById(R.id.profile_picture);
        btn_registrar = findViewById(R.id.btn_registrar);

        picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(hasCamera()){
                    startRecording(v);
                }
                else{
                    Toast.makeText(MainActivity.this, "No Camera Found", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn_registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newUser(email.getText().toString(),password.getText().toString());
                //mAuth.signOut();
            }
        });


    }


    @Override
    protected void onStart() {
        super.onStart();
        user = mAuth.getCurrentUser();
        //CheckSignIn();
    }

    private void CheckSignIn(String email, String password) {
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d("TAG", "signInWithEmail:success");
                                user = mAuth.getCurrentUser();
                                Toast.makeText(MainActivity.this, "Funcionó", Toast.LENGTH_SHORT).show();
                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w("TAG", "signInWithEmail:failure", task.getException());
                                Toast.makeText(MainActivity.this, "Authentication failed.",
                                        Toast.LENGTH_SHORT).show();

                            }

                            // ...
                        }
                    });
        }






    // To catch when the picture has been taken
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PHOTO_CAPTURE && resultCode == RESULT_OK){
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            picture.setImageBitmap(photo);
            //pictureUri= data.getData ();

            Toast.makeText (this , " Video saved to :\n" +
                    photo, Toast.LENGTH_LONG).show ();

        }
        else if ( resultCode == RESULT_CANCELED ) {
            Toast . makeText (this , " Video recording cancelled .",
                    Toast . LENGTH_LONG ) . show () ;
        }
        else {
            Toast.makeText(this , " Failed to record video ",
                    Toast.LENGTH_LONG ).show () ;
        }

    }


    // To know if the device has a camera or not.

    private boolean hasCamera () {
        return ( getPackageManager () . hasSystemFeature (
                PackageManager. FEATURE_CAMERA_ANY ) ) ;
    }


    //To open the view and take a picture.
    public void startRecording ( View view )
    {
        Intent intent = new Intent ( MediaStore. ACTION_IMAGE_CAPTURE ) ;
        startActivityForResult ( intent , PHOTO_CAPTURE ) ;
    }


    // to add a new user.
    public void addUser(){
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("User");
        User newUser = new User(username.getText().toString(),
                email.getText().toString(),
                password.getText().toString());
        myRef.child(newUser.getEmail()).setValue(newUser);

        FirebaseStorage storageRef = FirebaseStorage.getInstance();
        StorageReference storageReference =  storageRef.getReferenceFromUrl("gs://lab2-8db1f.appspot.com/");
        final StorageReference photoReference = storageReference.child("photos")
                .child(pictureUri.getLastPathSegment());
        photoReference.putFile(pictureUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content
                        //Uri downloadUrl = taskSnapshot.getDownloadUrl();

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        // ...
                        Log.d("TAG", "Fallo por :" + exception.toString());
                        Toast.makeText(getApplicationContext(),
                                exception.toString(), Toast.LENGTH_SHORT).show();                    }
                });


    }

    public void newUser(final String email, final String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("TAG", "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            insertarData();
                            Toast.makeText(MainActivity.this, "Agregado Exitosamente", Toast.LENGTH_SHORT).show();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("TAG", "createUserWithEmail:failure", task.getException());
                            //Toast.makeText(MainActivity.this, task.getException().getMessage().toString(), Toast.LENGTH_SHORT).show();
                            if(task.getException().getMessage()=="The email address is already in use by another account."){

                                new AlertDialog.Builder(MainActivity.this)
                                        .setTitle("Atención")
                                        .setMessage("Ya se encuentra un registro con ese nombre, ¿ desea sobreescribirlo ?")
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                FirebaseDatabase database = FirebaseDatabase.getInstance();
                                                DatabaseReference myRef = database.getReference("User");
                                                CheckSignIn(email,password);
                                                myRef.child(user.getUid());
                                                insertarData();
                                            }
                                        })
                                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {

                                                // do nothing
                                            }
                                        })
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();
                                return;

                            }
                        }
                    }
                });
    }



    public void insertarData(){


         FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("User");

        // Toast.makeText(getApplicationContext(),password.getText().toString() , Toast.LENGTH_SHORT).show();
        User newUser = new User(username.getText().toString(),
                email.getText().toString(),
                password.getText().toString());

        myRef.child(user.getUid()).setValue(newUser);

        FirebaseStorage storageRef = FirebaseStorage.getInstance();
        StorageReference storageReference = storageRef.getReferenceFromUrl("gs://lab2-8db1f.appspot.com/");




        final StorageReference photoReference = storageReference.child("photos")
                .child(user.getUid()+".jpg");

        picture.setDrawingCacheEnabled(true);
        picture.buildDrawingCache();
        Bitmap bitmap = picture.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = photoReference.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
            }
        });
    }


}
